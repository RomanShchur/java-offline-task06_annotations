package controller;

import jdk.nashorn.internal.ir.SwitchNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class MainClass {
  private static Logger logga = LogManager.getLogger();
  public static void main(String[] args)
    throws ClassNotFoundException, InvocationTargetException,
    IllegalAccessException {
    TestClass TC = new TestClass();
    Class tc = TC.getClass();
    Integer someInt = 20;
    String someStr = "String";
    char someChar = 'a';
    logga.info(" === Variables === ");
    for (Field field : tc.getDeclaredFields()) {
      field.setAccessible(true);
      Class type = field.getType();
      String name = field.getName();
      Annotation[] annotations = field.getDeclaredAnnotations();
      logga.info("type = " + type + " name = " + name);
      //      field.set(tc, someInt);
      //      Integer value = field.getInt(field);
      //      logga.info(value);
    }
    logga.info(" === Methods === ");
    for (Method method : tc.getDeclaredMethods()) {
      method.setAccessible(true);
      String name = method.getName();
      Class rType = method.getReturnType();
      Class[] parameters = method.getParameterTypes();
      Annotation[] annotations = method.getDeclaredAnnotations();
      logga.info("name = " + name + " returns = " + rType);
      for (Class parameter : parameters) {
        logga.info("needs " + parameter);
      }
      //      logga.info("Here could method be called");
      //      method.invoke(method.getName(), someStr);
      //      logga.info("\n");
    }
  }
}
