package controller;

import Annotations.MethodAnnot;
import Annotations.VarAnnot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestClass {
  private static Logger logga = LogManager.getLogger();
  @VarAnnot (name = "var1")
  private int someInt1 = 42;
  @VarAnnot (name = "var2")
  private Integer someInt2 = 1024;
  private String someString1 = "String";
  @MethodAnnot (name = "method1")
  private Integer method1(String ... args) {
    logga.info("method1 called");
    return args.length;
  }
  @MethodAnnot (name = "method1")
  private int method1(Integer inInt) {
    logga.info("method2 called");
    return 0;
  }
  @MethodAnnot (name = "method3")
  private String method3(char inChar) {
    logga.info("method3 called");
    return "" + inChar;
  }
}
